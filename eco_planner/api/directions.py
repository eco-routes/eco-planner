from flask import current_app

from .. import routing


def search(
    origin_lat, origin_lon,
    destination_lat, destination_lon,
    algorithm, metric,
):
    coords = ((origin_lat, origin_lon), (destination_lat, destination_lon))

    if algorithm == 'openrouteservice':
        routing_service = routing.OpenRouteService(current_app.config['API_KEY'])
    elif algorithm == 'shortest_path':
        routing_service = routing.PollutionRouting(
            current_app.config['MAP_DATA_PATH'],
            metric=metric,
        )

    with routing_service as router:
        route = router.directions(*coords)
    return route
