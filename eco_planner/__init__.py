import connexion
from connexion.resolver import RestyResolver
from flask_dotenv import DotEnv
from flask_cors import CORS


def create_app(test_config=None):
    api_app = connexion.FlaskApp(__name__)
    api_app.add_api('api.yml', resolver=RestyResolver('eco_planner.api'))

    app = api_app.app
    DotEnv(app)
    CORS(app)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(port=8080)
