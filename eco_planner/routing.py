import abc
import enum
from functools import wraps

import networkx as nx
import openrouteservice
import osmnx as ox
from more_itertools import pairwise
from openrouteservice import convert


def requires_context(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if self.client is None:
            raise RuntimeError('Called outside of context manager')
        return func(self, *args, **kwargs)
    return wrapper


class RoutingService(abc.ABC):
    @abc.abstractmethod
    def directions(self, origin, destination):
        pass


class OpenRouteService:
    def __init__(self, api_key: str) -> None:
        self.api_key = api_key

    def __enter__(self) -> 'OpenRouteService':
        self.client = openrouteservice.Client(key=self.api_key)
        return self

    def __exit__(self, *args, **kwargs) -> None:
        pass

    @requires_context
    def directions(self, origin, destination):
        """
        Calculate route between two points.
        """
        coords = (origin, destination)
        response = self.client.directions(coords)
        route_data = response['routes'][0]
        geometry = route_data['geometry']
        route = convert.decode_polyline(geometry)
        return {
            'duration': route_data['summary']['duration'],
            'length': route_data['summary']['distance'],
            'route': route,
        }


class PollutionRouting(RoutingService):
    class Algorithm(enum.Enum):
        SHORTEST_PATH = 'short'

    def __init__(
        self,
        path: str,
        metric: str = 'pollution_metric',
        algorithm: Algorithm = Algorithm.SHORTEST_PATH,
    ) -> None:
        self.path = path
        self.weight = metric
        self.algorithm = algorithm

    def __enter__(self):
        self.G = nx.read_gpickle(self.path)
        return self

    def __exit__(self, *args, **kwargs) -> None:
        pass

    # TODO ensure __enter__ has been called
    # TODO validate origin and destination => We only support Bristol
    def directions(self, origin, destination):
        """
        Calculate route between two points.
        """
        origin = origin[::-1]
        destination = destination[::-1]

        origin_node = self.get_nearest_node(origin)
        destination_node = self.get_nearest_node(destination)

        shortest_path = self.get_shortest_path(
            origin_node, destination_node,
        )

        if len(shortest_path) < 2:
            raise ValueError('No route found')

        duration = self.get_duration_for_path(shortest_path)
        distance = self.get_distance_for_path(shortest_path)
        pollution = self.get_pollution_for_path(shortest_path)
        polyline = self.convert_path_to_polyline(shortest_path)

        return {
            'duration': duration,
            'length': distance,
            'pollution': pollution,
            'route': polyline,
        }

    def get_nearest_node(self, coords):
        return ox.get_nearest_node(self.G, coords)

    def get_shortest_path(
        self, origin, destination
    ):
        if self.algorithm == PollutionRouting.Algorithm.SHORTEST_PATH:
            path = nx.shortest_path(
                self.G,
                origin, destination,
                weight=self.weight,
            )
        else:
            raise NotImplementedError()
        return path

    def convert_path_to_polyline(self, path):
        polyline = [
            (self.G.node[node_id]['x'], self.G.node[node_id]['y'])
            for node_id in path
        ]

        return {
            'type': 'PolyLine',
            'coordinates': polyline,
        }

    def get_duration_for_path(self, path):
        # TODO remove duplicated code
        edges = self.get_edges_for_path(path)
        return sum(edge['time_seconds'] for edge in edges)

    def get_distance_for_path(self, path):
        edges = self.get_edges_for_path(path)
        return sum(edge['length'] for edge in edges)

    def get_pollution_for_path(self, path):
        edges = self.get_edges_for_path(path)
        return sum(edge['pollution_metric'] for edge in edges)

    def get_edges_for_path(self, path):
        for origin, destination in pairwise(path):
            yield self.get_edge(origin, destination)

    def get_edge(self, origin, destination):
        for edge in self.G[origin][destination].values():
            if 'pollution_metric' in edge:
                return edge
        raise RuntimeError(f'Could not find edge for ({origin}, {destination})')
