# eco-planner


# Requirements

- poetry
- python 3.6
- libspatialindex


# Development setup

- clone the repository
- `cd eco-planner`
- create a virtual environment `python -m venv .venv`
- install all dependencies: `poetry install`
- configure the application, by creating a `.env` file (see `env.example` for more information)

Start the local shell

        poetry shell

To start the flask server run

        FLASK_APP="eco_planner" FLASK_ENV=development flask run

To run all tests run

        pytest
